//ex1
function calcExercise1() {
  var totalSum = 0;
  var n = 0;
  while (totalSum < 10000) {
    n++;
    totalSum += n;
  }
  document.getElementById(
    "result-ex1"
  ).innerHTML = `Số nguyên dương nhỏ nhất: ${n}`;
}
//ex2
function calcExercise2() {
  var x = document.getElementById("x").value * 1;
  var n = document.getElementById("n").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  document.getElementById("result-ex2").innerHTML = `Tổng: ${sum}`;
}
function calcExercise3() {
  var number_ex3 = document.getElementById("number-ex3").value * 1;
  result = 1;
  for (var i = 1; i <= number_ex3; i++) {
    result = result * i;
  }
  document.getElementById(
    "result-ex3"
  ).innerHTML = `Kết quả: ${number_ex3}! = ${result}`;
}
const btn_ex4 = document.getElementById("btn-ex4");
btn_ex4.addEventListener("click", calcExercise4);

function calcExercise4() {
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      var div = document.createElement("div");
      div.innerHTML = `Div chẵn`;
      div.style.backgroundColor = "blue";
      div.style.fontSize = "1.5rem";
      div.style.color = "#fff";
      div.style.padding = "0.5rem";
      document.getElementById("result-ex4").appendChild(div);
    } else {
      var div = document.createElement("div");
      div.innerHTML = `Div lẻ`;
      div.style.backgroundColor = "red";
      div.style.fontSize = "1.5rem";
      div.style.color = "#fff";
      div.style.padding = "0.5rem";
      document.getElementById("result-ex4").appendChild(div);
    }
  }
  btn_ex4.removeEventListener("click", calcExercise4);
}
